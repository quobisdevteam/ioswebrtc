//
//  Plugin.swift
//  iOSWebRTC
//
//  Created by Jose Lopez on 19/5/16.
//  Copyright © 2016 Quobis Networks Ltd. All rights reserved.
//

import Foundation

@objc public protocol PluginDelegate {
    func nativeToWeb(_ data: String, callbackId: String)
}

@objc open class Plugin: NSObject, CDVCommandDelegate {
    fileprivate var _view: UIView!
    fileprivate var _plugin: iosrtcPlugin!
    fileprivate var _delegate: PluginDelegate?

    // MARK: - Singleton
    
    @objc static open let sharedInstance = Plugin()

    fileprivate override init() { }

    // MARK: - Properties

    @objc open func set(view: UIView?) -> Plugin {
        /* No changes, do nothing */
        if (self._view == view) {
            return self
        }
        if (view != nil) {
            self._plugin = iosrtcPlugin(view: view!)
            self._plugin.commandDelegate = self
            self._plugin.pluginInitialize()
        } else {
            self._plugin = nil
        }
        self._view = view
        return self
    }

    @objc open func hasDelegate() -> Bool {
        return self._delegate != nil
    }

    @objc open func set(delegate: PluginDelegate) -> Plugin {
        self._delegate = delegate
        return self
    }

    @objc open func execute(command serialized: String) -> Bool {
        NSLog("execute", serialized)
        let jsonEntry = serialized.cdv_JSONObject()
        let command = CDVInvokedUrlCommand.init(fromJson: jsonEntry as! [AnyObject])
        let methodName = String.localizedStringWithFormat("%@:", (command?.methodName)!)
        let selector = NSSelectorFromString(methodName)
        if (self._plugin.responds(to: selector)) {
            NSLog("exec %@", methodName)
            self._plugin.perform(selector, with: command)
            return true
        }
        NSLog("Error, method %@ not defined in plugin", methodName)
        return false
    }

    // MARK: - CDVCommandDelegate

    @objc open func send(_ result: CDVPluginResult!, callbackId: String!) {
        let serialized = result.toJSONString()
        self._delegate?.nativeToWeb(serialized!, callbackId: callbackId)
    }

  }
