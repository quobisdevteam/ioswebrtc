//
//  CDVPlugin.h
//  iOSWebRTC
//
//  Created by Jose Lopez on 19/5/16.
//  Copyright © 2016 Quobis Networks Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


@class CDVPluginResult;


#pragma mark - CDVCommandDelegate protocol

@protocol CDVCommandDelegate
- (void)sendPluginResult:(CDVPluginResult*)result callbackId:(NSString*)callbackId;
@end

#pragma mark - CDVPlugin class

@interface CDVPlugin : NSObject {}

@property (nonatomic, weak) UIView* webView;
@property (nonatomic, weak) id <CDVCommandDelegate> commandDelegate;

- (CDVPlugin*)initWithView:(UIView*)theView;
- (void)pluginInitialize;
- (void)onAppTerminate;
- (void)onReset;
- (void)dispose;

@end


#pragma mark - CDVInvokedUrlCommand class

@interface CDVInvokedUrlCommand : NSObject{
    NSString* _callbackId;
    NSString* _className;
    NSString* _methodName;
    NSArray* _arguments;
}

@property (nonatomic, readonly) NSArray* arguments;
@property (nonatomic, readonly) NSString* callbackId;
@property (nonatomic, readonly) NSString* className;
@property (nonatomic, readonly) NSString* methodName;

+ (CDVInvokedUrlCommand*)commandFromJson:(NSArray*)jsonEntry;

- (id)initWithArguments:(NSArray*)arguments
             callbackId:(NSString*)callbackId
              className:(NSString*)className
             methodName:(NSString*)methodName;

- (id)initFromJson:(NSArray*)jsonEntry;

- (id)argumentAtIndex:(NSUInteger)index;

@end


#pragma mark - CDVCommandStatus enum

typedef enum {
    CDVCommandStatus_NO_RESULT = 0,
    CDVCommandStatus_OK,
    CDVCommandStatus_CLASS_NOT_FOUND_EXCEPTION,
    CDVCommandStatus_ILLEGAL_ACCESS_EXCEPTION,
    CDVCommandStatus_INSTANTIATION_EXCEPTION,
    CDVCommandStatus_MALFORMED_URL_EXCEPTION,
    CDVCommandStatus_IO_EXCEPTION,
    CDVCommandStatus_INVALID_ACTION,
    CDVCommandStatus_JSON_EXCEPTION,
    CDVCommandStatus_ERROR
} CDVCommandStatus;


#pragma mark - CDVPluginResult class

@interface CDVPluginResult : NSObject {}

@property (nonatomic, strong, readonly) NSNumber* status;
@property (nonatomic, strong, readonly) id message;
@property (nonatomic, strong) NSNumber* keepCallback;
@property (nonatomic, strong) id associatedObject;

- (CDVPluginResult*)init;
+ (CDVPluginResult*)resultWithStatus:(CDVCommandStatus)statusOrdinal;
+ (CDVPluginResult*)resultWithStatus:(CDVCommandStatus)statusOrdinal messageAsString:(NSString*)theMessage;
+ (CDVPluginResult*)resultWithStatus:(CDVCommandStatus)statusOrdinal messageAsDictionary:(NSDictionary*)theMessage;
+ (CDVPluginResult*)resultWithStatus:(CDVCommandStatus)statusOrdinal messageAsArrayBuffer:(NSData*)theMessage;

- (void)setKeepCallbackAsBool:(BOOL)bKeepCallback;
- (NSString*)toJSONString;
- (NSString*)argumentsAsJSON;

@end


#pragma mark - NSArray extension

@interface NSArray (CDVJSONSerializingPrivate)
- (NSString*)cdv_JSONString;
@end


#pragma mark - NSDictionary extension

@interface NSDictionary (CDVJSONSerializingPrivate)
- (NSString*)cdv_JSONString;
@end


#pragma mark - NSString extension

@interface NSString (CDVJSONSerializingPrivate)
- (id)cdv_JSONObject;
@end
