//
//  iOSWebRTC.h
//  iOSWebRTC
//
//  Created by Jose Lopez on 18/5/16.
//  Copyright © 2016 Quobis Networks Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

//! Project version number for iOSWebRTC.
FOUNDATION_EXPORT double iOSWebRTCVersionNumber;

//! Project version string for iOSWebRTC.
FOUNDATION_EXPORT const unsigned char iOSWebRTCVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <iOSWebRTC/PublicHeader.h>

// #import "RTCAudioSource.h"
#import "RTCAVFoundationVideoSource.h"
#import "RTCAudioTrack.h"
#import "RTCDataChannel.h"
#import "RTCEAGLVideoView.h"
// #import "RTCI420Frame.h"
#import "RTCICECandidate.h"
#import "RTCICEServer.h"
#import "RTCMediaConstraints.h"
// #import "RTCMediaSource.h"
#import "RTCMediaStream.h"
#import "RTCMediaStreamTrack.h"
// #import "RTCNSGLVideoView.h"
// #import "RTCOpenGLVideoRenderer.h"
#import "RTCPair.h"
#import "RTCPeerConnection.h"
#import "RTCPeerConnectionDelegate.h"
#import "RTCPeerConnectionFactory.h"
#import "RTCPeerConnectionInterface.h"
#import "RTCSessionDescription.h"
#import "RTCSessionDescriptionDelegate.h"
// #import "RTCStatsDelegate.h"
// #import "RTCStatsReport.h"
#import "RTCTypes.h"
#import "RTCVideoCapturer.h"
#import "RTCVideoRenderer.h"
#import "RTCVideoSource.h"
#import "RTCVideoTrack.h"

#import "CDV.h"
