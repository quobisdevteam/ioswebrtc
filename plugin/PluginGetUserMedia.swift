import Foundation
import AVFoundation


class PluginGetUserMedia {
	var rtcPeerConnectionFactory: RTCPeerConnectionFactory


	init(rtcPeerConnectionFactory: RTCPeerConnectionFactory) {
		NSLog("PluginGetUserMedia#init()")

		self.rtcPeerConnectionFactory = rtcPeerConnectionFactory
	}


	func call(
		_ constraints: NSDictionary,
		callback: (_ data: NSDictionary) -> Void,
		errback: (_ error: String) -> Void,
		eventListenerForNewStream: (_ pluginMediaStream: PluginMediaStream) -> Void
	) {
		NSLog("PluginGetUserMedia#call()")

		let	audioRequested = constraints.object(forKey: "audio") as? Bool ?? false
		let	videoRequested = constraints.object(forKey: "video") as? Bool ?? false
		let	videoDeviceId = constraints.object(forKey: "videoDeviceId") as? String

		var rtcMediaStream: RTCMediaStream
		var pluginMediaStream: PluginMediaStream?
		var rtcAudioTrack: RTCAudioTrack?
		var rtcVideoTrack: RTCVideoTrack?
		var rtcVideoCapturer: RTCVideoCapturer?
		var rtcVideoSource: RTCVideoSource?
		var videoDevice: AVCaptureDevice?

		if videoRequested == true {
            switch AVCaptureDevice.authorizationStatus(for: AVMediaType.video) {
			case AVAuthorizationStatus.notDetermined:
				NSLog("PluginGetUserMedia#call() | video authorization: not determined")
			case AVAuthorizationStatus.authorized:
				NSLog("PluginGetUserMedia#call() | video authorization: authorized")
			case AVAuthorizationStatus.denied:
				NSLog("PluginGetUserMedia#call() | video authorization: denied")
				errback("video denied")
				return
			case AVAuthorizationStatus.restricted:
				NSLog("PluginGetUserMedia#call() | video authorization: restricted")
				errback("video restricted")
				return
			}
		}

		if audioRequested == true {
            switch AVCaptureDevice.authorizationStatus(for: AVMediaType.audio) {
			case AVAuthorizationStatus.notDetermined:
				NSLog("PluginGetUserMedia#call() | audio authorization: not determined")
			case AVAuthorizationStatus.authorized:
				NSLog("PluginGetUserMedia#call() | audio authorization: authorized")
			case AVAuthorizationStatus.denied:
				NSLog("PluginGetUserMedia#call() | audio authorization: denied")
				errback("audio denied")
				return
			case AVAuthorizationStatus.restricted:
				NSLog("PluginGetUserMedia#call() | audio authorization: restricted")
				errback("audio restricted")
				return
			}
		}

		rtcMediaStream = self.rtcPeerConnectionFactory.mediaStream(withLabel: UUID().uuidString)

		if videoRequested == true {
			// No specific video device requested.
			if videoDeviceId == nil {
				NSLog("PluginGetUserMedia#call() | video requested (device not specified)")

                for device: AVCaptureDevice in (AVCaptureDevice.devices(for: AVMediaType.video)) {
                    if device.position == AVCaptureDevice.Position.front {
						videoDevice = device
						break
					}
				}
			}

			// Video device specified.
			else {
				NSLog("PluginGetUserMedia#call() | video requested (specified device id: \(videoDeviceId!))")

                for device: AVCaptureDevice in (AVCaptureDevice.devices(for: AVMediaType.video) ) {
					if device.uniqueID == videoDeviceId {
						videoDevice = device
						break
					}
				}
			}

			if videoDevice == nil {
				NSLog("PluginGetUserMedia#call() | video requested but no suitable device found")

				errback("no suitable camera device found")
				return
			}

			NSLog("PluginGetUserMedia#call() | chosen video device: \(videoDevice!)")

			rtcVideoCapturer = RTCVideoCapturer(deviceName: videoDevice!.localizedName)

			rtcVideoSource = self.rtcPeerConnectionFactory.videoSource(with: rtcVideoCapturer,
				constraints: RTCMediaConstraints()
			)

			rtcVideoTrack = self.rtcPeerConnectionFactory.videoTrack(withID: UUID().uuidString,
				source: rtcVideoSource
			)

			rtcMediaStream.addVideoTrack(rtcVideoTrack)
		}

		if audioRequested == true {
			NSLog("PluginGetUserMedia#call() | audio requested")

			rtcAudioTrack = self.rtcPeerConnectionFactory.audioTrack(withID: UUID().uuidString)

			rtcMediaStream.addAudioTrack(rtcAudioTrack!)
		}

		pluginMediaStream = PluginMediaStream(rtcMediaStream: rtcMediaStream)
		pluginMediaStream!.run()

		// Let the plugin store it in its dictionary.
		eventListenerForNewStream(pluginMediaStream!)

		callback([
			"stream": pluginMediaStream!.getJSON()
		])
	}
}
