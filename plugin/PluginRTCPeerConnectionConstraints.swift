import Foundation


class PluginRTCPeerConnectionConstraints {
	fileprivate var constraints: RTCMediaConstraints


	init(pcConstraints: NSDictionary?) {
		NSLog("PluginRTCPeerConnectionConstraints#init()")

		if pcConstraints == nil {
			self.constraints = RTCMediaConstraints()
			return
		}
        
		var	offerToReceiveAudio = pcConstraints?.object(forKey: "offerToReceiveAudio") as? Bool
		var	offerToReceiveVideo = pcConstraints?.object(forKey: "offerToReceiveVideo") as? Bool

        if offerToReceiveAudio == nil {
            offerToReceiveAudio = pcConstraints?.object(forKey: "OfferToReceiveAudio") as? Bool
        }
        if offerToReceiveVideo == nil {
            offerToReceiveVideo = pcConstraints?.object(forKey: "OfferToReceiveVideo") as? Bool
        }
        let mandatory = pcConstraints?.object(forKey: "mandatory") as? NSDictionary
        if offerToReceiveAudio == nil {
            offerToReceiveAudio = mandatory?.object(forKey: "offerToReceiveAudio") as? Bool
        }
        if offerToReceiveAudio == nil {
            offerToReceiveAudio = mandatory?.object(forKey: "OfferToReceiveAudio") as? Bool
        }
        if offerToReceiveVideo == nil {
            offerToReceiveVideo = mandatory?.object(forKey: "offerToReceiveVideo") as? Bool
        }
        if offerToReceiveVideo == nil {
            offerToReceiveVideo = mandatory?.object(forKey: "OfferToReceiveVideo") as? Bool
        }
        
		if offerToReceiveAudio == nil && offerToReceiveVideo == nil {
			self.constraints = RTCMediaConstraints()
			return
		}
        
		if offerToReceiveAudio == nil {
			offerToReceiveAudio = false
		}

		if offerToReceiveVideo == nil {
			offerToReceiveVideo = false
		}

		NSLog("PluginRTCPeerConnectionConstraints#init() | [offerToReceiveAudio:\(offerToReceiveAudio!) | offerToReceiveVideo:\(offerToReceiveVideo!)]")

		self.constraints = RTCMediaConstraints(
			mandatoryConstraints: [
				RTCPair(key: "OfferToReceiveAudio", value: offerToReceiveAudio == true ? "true" : "false"),
				RTCPair(key: "OfferToReceiveVideo", value: offerToReceiveVideo == true ? "true" : "false")
			],
			optionalConstraints: []
		)
	}


	func getConstraints() -> RTCMediaConstraints {
		NSLog("PluginRTCPeerConnectionConstraints#getConstraints()")

		return self.constraints
	}
}
