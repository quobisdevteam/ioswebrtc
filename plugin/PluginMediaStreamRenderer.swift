import Foundation
import AVFoundation

struct Mask: Hashable {
    var rect: CGRect
    var image: CGImage
    var hashValue: Int
    var zIndex: Int
}
func ==(left: Mask, right: Mask) -> Bool {
    return left.hashValue == right.hashValue
}

class PluginMediaStreamRenderer: NSObject, RTCEAGLVideoViewDelegate {
    var webView: UIView
    var eventListener: (_ data: NSDictionary) -> Void
    var elementView: UIView
    var videoView: RTCEAGLVideoView
    var pluginMediaStream: PluginMediaStream?
    var rtcAudioTrack: RTCAudioTrack?
    var rtcVideoTrack: RTCVideoTrack?
    /* vvv Quobis */
    static var masks: Set<Mask> = []
    var mask: Mask!
    /* ^^^ Quobis */
    
    init(
        webView: UIView,
        eventListener: @escaping (_ data: NSDictionary) -> Void
        ) {
        NSLog("PluginMediaStreamRenderer#init()")
        
        // The browser HTML view.
        self.webView = webView
        self.eventListener = eventListener
        //self.webView.opaque = false
        //self.webView.backgroundColor = UIColor.clearColor()
        
        // The video element view.
        self.elementView = UIView()
        // The effective video view in which the the video stream is shown.
        // It's placed over the elementView.
        self.videoView = RTCEAGLVideoView()
        
        self.webView.superview?.addSubview(self.elementView)
        self.elementView.isUserInteractionEnabled = false
        self.elementView.isHidden = true
        self.elementView.backgroundColor = UIColor.clear
        self.elementView.addSubview(self.videoView)
        self.elementView.layer.masksToBounds = true
        
        self.videoView.isUserInteractionEnabled = false
    }
    
    
    func run() {
        NSLog("PluginMediaStreamRenderer#run()")
        
        self.videoView.delegate = self
    }
    
    
    func render(_ pluginMediaStream: PluginMediaStream) {
        NSLog("PluginMediaStreamRenderer#render()")
        
        if self.pluginMediaStream != nil {
            self.reset()
        }
        
        self.pluginMediaStream = pluginMediaStream
        
        // Take the first audio track.
        for (_, track) in pluginMediaStream.audioTracks {
            self.rtcAudioTrack = track.rtcMediaStreamTrack as? RTCAudioTrack
            break
        }
        
        // Take the first video track.
        for (_, track) in pluginMediaStream.videoTracks {
            self.rtcVideoTrack = track.rtcMediaStreamTrack as? RTCVideoTrack
            break
        }
        
        if self.rtcVideoTrack != nil {
            self.rtcVideoTrack!.add(self.videoView)
        }
    }
    
    
    func mediaStreamChanged() {
        NSLog("PluginMediaStreamRenderer#mediaStreamChanged()")
        
        if self.pluginMediaStream == nil {
            return
        }
        
        let oldRtcVideoTrack: RTCVideoTrack? = self.rtcVideoTrack
        
        self.rtcAudioTrack = nil
        self.rtcVideoTrack = nil
        
        // Take the first audio track.
        for (_, track) in self.pluginMediaStream!.audioTracks {
            self.rtcAudioTrack = track.rtcMediaStreamTrack as? RTCAudioTrack
            break
        }
        
        // Take the first video track.
        for (_, track) in pluginMediaStream!.videoTracks {
            self.rtcVideoTrack = track.rtcMediaStreamTrack as? RTCVideoTrack
            break
        }
        
        // If same video track as before do nothing.
        if oldRtcVideoTrack != nil && self.rtcVideoTrack != nil &&
            oldRtcVideoTrack!.label == self.rtcVideoTrack!.label {
            NSLog("PluginMediaStreamRenderer#mediaStreamChanged() | same video track as before")
        }
            
            // Different video track.
        else if oldRtcVideoTrack != nil && self.rtcVideoTrack != nil &&
            oldRtcVideoTrack!.label != self.rtcVideoTrack!.label {
            NSLog("PluginMediaStreamRenderer#mediaStreamChanged() | has a new video track")
            
            oldRtcVideoTrack!.remove(self.videoView)
            self.rtcVideoTrack!.add(self.videoView)
        }
            
            // Did not have video but now it has.
        else if oldRtcVideoTrack == nil && self.rtcVideoTrack != nil {
            NSLog("PluginMediaStreamRenderer#mediaStreamChanged() | video track added")
            
            self.rtcVideoTrack!.add(self.videoView)
        }
            
            // Had video but now it has not.
        else if oldRtcVideoTrack != nil && self.rtcVideoTrack == nil {
            NSLog("PluginMediaStreamRenderer#mediaStreamChanged() | video track removed")
            
            oldRtcVideoTrack!.remove(self.videoView)
        }
    }
    
    func maskForRect(_ rect: CGRect, view: UIView) -> Mask? {
        /* Get a snapshot of view area */
        if (rect.size.width == 0) {
            return nil
        }
        var graphicsContext: CGContext!
        UIGraphicsBeginImageContext(rect.size)
        graphicsContext = UIGraphicsGetCurrentContext()
        graphicsContext.translateBy(x: -rect.origin.x, y: -rect.origin.y)
        view.layer.render(in: graphicsContext)
        let snapshot = CIImage(cgImage: (UIGraphicsGetImageFromCurrentImageContext()?.cgImage!)!)
        
        /* Apply monochrome filter to convert any non-white color to black */
        let monochromeFilter: CIFilter! = CIFilter(name: "CIColorMonochrome")
        monochromeFilter.setDefaults()
        monochromeFilter.setValue(snapshot, forKey: kCIInputImageKey)
        monochromeFilter.setValue(CIColor(cgColor: UIColor.black.cgColor), forKey: "inputColor")
        monochromeFilter.setValue(NSNumber(value: 1 as Int32), forKey: "inputIntensity")
        
        /* Invert colors: view area white colors are now black, and ready to apply an alpha filter */
        let invertFilter: CIFilter! = CIFilter(name: "CIColorInvert")
        invertFilter.setDefaults()
        invertFilter.setValue(monochromeFilter.outputImage, forKey: kCIInputImageKey)
        
        /* Convert CIImage to CGImage and return a Mask instance */
        let imageContext: CIContext! = CIContext(options:nil)
        let ciimage: CIImage! = invertFilter.outputImage
        let image: CGImage = imageContext.createCGImage(ciimage, from: ciimage.extent)!
        return Mask(rect: rect, image: image, hashValue: self.elementView.hashValue, zIndex: 0)
    }
    
    func clippedUIImageForRect(_ clipRect: CGRect, view: UIView) -> UIImage {
        var graphicsContext: CGContext!
        UIGraphicsBeginImageContext(clipRect.size)
        graphicsContext = UIGraphicsGetCurrentContext()
        graphicsContext.translateBy(x: -clipRect.origin.x, y: -clipRect.origin.y)
        view.layer.render(in: graphicsContext)
        let img: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        //UIGraphicsEndImageContext();
        return img
    }
    
    func getMaskCGImageFromCGImage(_ input: CGImage) -> CGImage {
        let monochromeFilter: CIFilter! = CIFilter(name: "CIColorMonochrome")
        monochromeFilter.setDefaults()
        monochromeFilter.setValue(CIImage(cgImage: input), forKey: kCIInputImageKey)
        monochromeFilter.setValue(CIColor(cgColor: UIColor.black.cgColor), forKey: "inputColor")
        monochromeFilter.setValue(NSNumber(value: 1 as Int32), forKey: "inputIntensity")
        
        let invertFilter: CIFilter! = CIFilter(name: "CIColorInvert")
        invertFilter.setDefaults()
        invertFilter.setValue(monochromeFilter.outputImage, forKey: kCIInputImageKey)
        
        let alphaFilter: CIFilter! = CIFilter(name: "CIMaskToAlpha")
        alphaFilter.setDefaults()
        alphaFilter.setValue(invertFilter.outputImage, forKey: kCIInputImageKey)
        
        let newImage: CIImage! = alphaFilter.outputImage
        let imageContext: CIContext! = CIContext(options:nil)
        return imageContext.createCGImage(newImage, from: newImage.extent)!
    }
    
    func refresh(_ data: NSDictionary) {
        
        // vvv Quobis
        do {
            
            for portDescription in AVAudioSession.sharedInstance().currentRoute.outputs {
                if !(convertFromAVAudioSessionPort(portDescription.portType) == convertFromAVAudioSessionPort(AVAudioSession.Port.headphones)) {
                    if #available(iOS 10.0, *) {
                        try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category(rawValue: convertFromAVAudioSessionCategory(AVAudioSession.Category.playAndRecord)), mode: .voiceChat)
                    } else {
                        AVAudioSession.sharedInstance().perform(NSSelectorFromString("setCategory:error:"), with: AVAudioSession.Category.playAndRecord)
                    }
                    try AVAudioSession.sharedInstance().overrideOutputAudioPort(AVAudioSession.PortOverride.speaker)
                }
            }
        } catch {
            NSLog("OutputSpeaker() error: \(error)")
        };
        // ^^^ Quobis
        
        //let windowWidth = (self.webView.stringByEvaluatingJavaScriptFromString("$(window).width()")! as NSString).integerValue;
        
        //let windowHeight = (self.webView.stringByEvaluatingJavaScriptFromString("$(window).height()")! as NSString).integerValue;
        let scaleW = Float(self.webView.frame.width)/Float(UIScreen.main.bounds.width)
        let scaleH = Float(self.webView.frame.height)/Float(UIScreen.main.bounds.height)
        
        let elementLeft = (data.object(forKey: "elementLeft") as? Float ?? 0) * scaleW
        let elementTop = (data.object(forKey: "elementTop") as? Float ?? 0) * scaleH
        let elementWidth = (data.object(forKey: "elementWidth") as? Float ?? 0) * scaleW
        let elementHeight = (data.object(forKey: "elementHeight") as? Float ?? 0) * scaleH
        var videoViewWidth = (data.object(forKey: "videoViewWidth") as? Float ?? 0) * scaleW
        var videoViewHeight = (data.object(forKey: "videoViewHeight") as? Float ?? 0) * scaleH
        let visible = data.object(forKey: "visible") as? Bool ?? true
        let opacity = data.object(forKey: "opacity") as? Float ?? 1
        let zIndex = data.object(forKey: "zIndex") as? Float ?? 0
        let mirrored = data.object(forKey: "mirrored") as? Bool ?? false
        let clip = data.object(forKey: "clip") as? Bool ?? true
        let borderRadius = data.object(forKey: "borderRadius") as? Float ?? 0
        
        NSLog("PluginMediaStreamRenderer#refresh() [elementLeft:\(elementLeft), elementTop:\(elementTop), elementWidth:\(elementWidth), elementHeight:\(elementHeight), videoViewWidth:\(videoViewWidth), videoViewHeight:\(videoViewHeight), visible:\(visible), opacity:\(opacity), zIndex:\(zIndex), mirrored:\(mirrored), clip:\(clip), borderRadius:\(borderRadius)]")
        
        let videoViewLeft: Float = (elementWidth - videoViewWidth) / 2
        let videoViewTop: Float = (elementHeight - videoViewHeight) / 2
        
        self.elementView.frame = CGRect(
            x: CGFloat(elementLeft),
            y: CGFloat(elementTop),
            width: CGFloat(elementWidth),
            height: CGFloat(elementHeight)
        )
        
        // vvv Quobis
        self.webView.layer.mask = nil
        /* Previous mask exists, remove it from masks list */
        if (nil != self.mask) {
            PluginMediaStreamRenderer.masks.remove(self.mask)
        }
        /* Get new mask and store it */
        if let maskRect = self.maskForRect(self.elementView.frame, view: self.webView) {
            self.mask = maskRect
            self.mask.zIndex = Int(zIndex)
            PluginMediaStreamRenderer.masks.insert(self.mask)
        }
        self.updateMask()
        // ^^^ Quobis
        
        // NOTE: Avoid a zero-size UIView for the video (the library complains).
        if videoViewWidth == 0 || videoViewHeight == 0 {
            videoViewWidth = 1
            videoViewHeight = 1
            self.videoView.isHidden = true
        } else {
            self.videoView.isHidden = false
        }
        
        self.videoView.frame = CGRect(
            x: CGFloat(videoViewLeft),
            y: CGFloat(videoViewTop),
            width: CGFloat(videoViewWidth),
            height: CGFloat(videoViewHeight)
        )
        
        if visible {
            self.elementView.isHidden = false
        } else {
            self.elementView.isHidden = true
        }
        
        self.elementView.alpha = CGFloat(opacity)
        
        // Storing the zIndex as Tag of view for sorting the view herarchy according to zIndex later
        self.elementView.tag = Int(zIndex)
        
        //Sorting the views according to their zIndex
        var prevSiblingView: UIView?
        for childView in (self.webView.superview?.subviews)! {
            if(childView.tag > Int(zIndex)) {
                prevSiblingView = childView
                break
            }
        }
        
        if (prevSiblingView == nil) {
            prevSiblingView = self.videoView
        }
        
        if let view = prevSiblingView {
            self.webView.superview?.insertSubview(self.elementView, belowSubview: view)
        }
        
        if !mirrored {
            self.elementView.transform = CGAffineTransform.identity
        } else {
            self.elementView.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
        }
        
        if clip {
            self.elementView.clipsToBounds = true
        } else {
            self.elementView.clipsToBounds = false
        }
        
        self.elementView.layer.cornerRadius = CGFloat(borderRadius)
        
    }
    
    
    func close() {
        NSLog("PluginMediaStreamRenderer#close()")
        
        self.reset()
        self.elementView.removeFromSuperview()
        /* vvv Quobis TODO */
        /* Previous mask exists, remove it from masks list */
        if (nil != self.mask) {
            PluginMediaStreamRenderer.masks.remove(self.mask)
            self.mask = nil
            self.updateMask()
        }
        
        do {
            try AVAudioSession.sharedInstance().overrideOutputAudioPort(AVAudioSession.PortOverride.none)
        } catch {
            NSLog("OutputSpeaker() error: \(error)")
        }
        /* ^^^ Quobis */
    }
    
    /* vvv Quobis */
    fileprivate func updateMask() {
        UIGraphicsBeginImageContextWithOptions(self.webView.frame.size, false, 0)
        let context = UIGraphicsGetCurrentContext()
        /* White as background -> mask will be transparent */
        context?.setFillColor(UIColor.white.cgColor)
        context?.fill(self.webView.frame)
        /* Draw any mask in context */
        for mask in PluginMediaStreamRenderer.masks {
            UIImage(cgImage: mask.image).draw(in: mask.rect)
        }
        /* Get full mask */
        let img: UIImage! = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        // DEBUG: UIImageWriteToSavedPhotosAlbum(img, nil, nil, nil)
        
        /* Apply alpha filter to mask */
        let alphaFilter: CIFilter! = CIFilter(name: "CIMaskToAlpha")
        alphaFilter.setDefaults()
        alphaFilter.setValue(CIImage(cgImage: img.cgImage!), forKey: kCIInputImageKey)
        
        let newImage: CIImage! = alphaFilter.outputImage
        let imageContext: CIContext! = CIContext(options:nil)
        let maskImage: CGImage = imageContext.createCGImage(newImage, from: newImage.extent)!
        
        let mask: CALayer! = CALayer()
        mask.contents = maskImage
        mask.contentsGravity = CALayerContentsGravity.resizeAspect
        mask.bounds = self.webView.bounds
        mask.anchorPoint = CGPoint(x: 0.5, y: 0.5)
        mask.position = CGPoint(x: Int(self.webView.frame.width/2), y: Int(self.webView.frame.height/2))
        self.webView.layer.mask = mask
        self.elementView.layer.mask = mask
    }
    /* ^^^ Quobis */
    
    /**
     * Private API.
     */
    
    
    fileprivate func reset() {
        NSLog("PluginMediaStreamRenderer#reset()")
        
        if self.rtcVideoTrack != nil {
            self.rtcVideoTrack!.remove(self.videoView)
        }
        
        self.pluginMediaStream = nil
        self.rtcAudioTrack = nil
        self.rtcVideoTrack = nil
    }
    
    
    /**
     * Methods inherited from RTCEAGLVideoViewDelegate.
     */
    
    
    func videoView(_ videoView: RTCEAGLVideoView!, didChangeVideoSize size: CGSize) {
        NSLog("PluginMediaStreamRenderer | video size changed [width:\(size.width), height:\(size.height)]")
        
        self.eventListener([
            "type": "videoresize",
            "size": [
                "width": Int(size.width),
                "height": Int(size.height)
            ]
            ])
    }
    
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromAVAudioSessionPort(_ input: AVAudioSession.Port) -> String {
	return input.rawValue
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromAVAudioSessionCategory(_ input: AVAudioSession.Category) -> String {
	return input.rawValue
}
