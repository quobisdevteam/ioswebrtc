import Foundation
import AVFoundation


/**
 * Doc: https://developer.apple.com/library/mac/documentation/AVFoundation/Reference/AVCaptureDevice_Class/index.html
 */


class PluginGetMediaDevices {
	class func call(_ callback: (_ data: NSDictionary) -> Void) {
		NSLog("PluginGetMediaDevices#call()")

        let devices = AVCaptureDevice.devices()

		let json: NSMutableDictionary = [
			"devices": NSMutableDictionary()
		]

		for device: AVCaptureDevice in devices {
			var position: String
            let hasAudio = device.hasMediaType(AVMediaType.audio)
            let hasVideo = device.hasMediaType(AVMediaType.video)

			switch device.position {
            case AVCaptureDevice.Position.unspecified:
				position = "unknown"
            case AVCaptureDevice.Position.back:
				position = "back"
            case AVCaptureDevice.Position.front:
				position = "front"
			}

			NSLog("- device [uniqueID:'\(device.uniqueID)', localizedName:'\(device.localizedName)', position:\(position), audio:\(hasAudio), video:\(hasVideo), connected:\(device.isConnected)")

			if device.isConnected == false || (hasAudio == false && hasVideo == false) {
				continue
			}

			(json["devices"] as! NSMutableDictionary)[device.uniqueID] = [
				"deviceId": device.uniqueID,
				"kind": hasAudio ? "audio" : "video",
				"label": device.localizedName
			]
		}

		callback(json)
	}
}
