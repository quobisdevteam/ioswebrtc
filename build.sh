#!/bin/bash

STYLE=html/style.js
MOCK=html/cordovaMock.js
PLUGIN=html/cordova-plugin-iosrtc.js
PLUGIN_PATCH=html/cordova-plugin-iosrtc.js.patch

OUTPUT=iOS-adapter-BBVA.js

function appendFile {
	echo "/* $1 */" >> $OUTPUT
	cat $1 >> $OUTPUT
}

# Write HEADER
cat > $OUTPUT << EOF
/* File automatically generated with build script */
/* $(date) */
/* Only for iOS devices and BBVA environment */
if (navigator.userAgent.match(/.*(iPhone|iPad).*/) != null) {

EOF

# Write Style
appendFile $STYLE

# Write Cordova Mock
appendFile $MOCK

# Copy Plugin
echo "/* $PLUGIN */" >> $OUTPUT
patch $PLUGIN -p0 -i $PLUGIN_PATCH -o - >> $OUTPUT 2>/dev/null

# Write FOOTER
cat >> $OUTPUT << EOF

}
EOF

# Worarround for browserify
sed -i 's/\<require\>/_require/g' $OUTPUT
