var cordova = window.cordova;

var interval = setInterval(function () {
	if(window.getComputedStyle($('.c2c-video')[0]).display == 'block') {
		cordova.plugins.iosrtc.refreshVideos();
		clearInterval(interval);
	} 
}, 5000);

if (!cordova) {
	window.webrtcNativeResponse = function(data) {
		if (data.action) {
			if (data.action ==  "webRTC" && data.data.callbackId) {
				var callback = cordova.callbackSuccess[data.data.callbackId];
				if (callback) {
					var response = JSON.parse(data.data.payload).message;
					callback(response);
					console.log(callback.toString());
					console.log(data);
				}
			}
		}
	}

	cordova = window.cordova = {
	    plugins: {},
	    callbackId: 0,
	    callbackSuccess: {},
	    callbackError: {},
	    define: function(name, fn) {
		var registry = {
		    "cordova/exec": function(onSuccess, onError, pluginName, methodName, args) {
			console.error("call " + pluginName + ':' + methodName + ' with args', args);
			cordova.callbackSuccess[cordova.callbackId] = onSuccess;

			if (onError){
			    cordova.callbackError[cordova.callbackId] = onError;
			}

			cordova.send({
			    type: "request",
			    action: "webRTC",
			    data: {
				action: "exec",
				payload: JSON.stringify([(cordova.callbackId++).toString(), pluginName, methodName, args])
			    },
			    timestamp: Date.now()
			});

		    }
		};
		var exports = {};
		fn(function(name) {
		    return registry[name];
		}, null, exports);
		cordova.plugins.iosrtc = exports.exports;
	    },

	    send: function(data) {
		var iframe = document.createElement('iframe');

		iframe.onload = function () {
		    iframe.parentNode.removeChild(iframe);
		};

		iframe.setAttribute('style', 'display:none');
		iframe.setAttribute('src', 'js-frame://' + JSON.stringify(data));

		document.body.appendChild(iframe);
	    }


	};
}

var define = cordova.define;
cordova.define = function(name, fn) {
	define(name, fn);
	if (name === 'cordova-plugin-iosrtc.Plugin') {
		cordova.plugins.iosrtc.registerGlobals();
	}
};
