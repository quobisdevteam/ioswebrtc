var addCSS = function(style) {
	var div = document.createElement('div');
	div.innerHTML = '<style>' + style + '</style>';
	document.body.appendChild(div.childNodes[0]);
}

var style = " \
video { \
	object-fit: fill; \
} \
video::-webkit-media-controls-start-playback-button { \
	display: none !important; \
	-webkit-appearance: none; \
} \
";

addCSS(style);

