### What is this repository for? ###

* Cordova-plugin-iosrtc -> https://github.com/BasqueVoIPMafia/cordova-plugin-iosrtc with cordova mock to avoid install it sending results from a custom delegate.

### Requirements ###

* Xcode 9
* iOS 9

### License ###

* MIT